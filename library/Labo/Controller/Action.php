<?php
class Labo_Controller_Action extends Zend_Controller_Action
{
    protected $_mod;
    public function init()
    {
        parent::init();
        $this->_mod = $this->_request->getModuleName();
        $dirtemp= APPLICATION_PATH . '/templates/'.$this->_mod.'/layouts/scripts';
        $option = array ('layout' => 'layout', 'layoutPath' =>
            $dirtemp);
        $layout = Zend_Layout::startMvc ( $option );
        $baseurl=$this->_request->getBaseUrl ();
        $dircss=$baseurl.'/application/templates/'.$this->_mod.'/layouts/scripts/css/styles.css';
        $dirimg=$baseurl.'/application/templates/'.$this->_mod.'/layouts/scripts/images';

        $this->view->headLink()->offsetSetStylesheet ( 0,
            $dircss,'all' );
        $this->view->headTitle ("Tiêu đề trang chủ");
        $this->view->dirimg=$dirimg;
         $this->view->baseurl=$baseurl;
    }
}