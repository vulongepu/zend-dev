<?php

class Employe_IndexController extends Labo_Controller_Action
{

    public function indexAction()
    {
        $employees = new Application_Model_Employe();
        $employees = $employees->getList()->toArray();
        $this->view->employees = $employees;
    }

}