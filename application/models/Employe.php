<?php

class Application_Model_Employe extends Zend_Db_Table_Abstract 
{

	protected $_name = 'employees';
	protected $_primary = 'emp_no';

	public function getList()
	{
		return $this->fetchAll();
	}

	public function createEmploye($arr)
	{
		$data = array(
		"last_name"  => $arr['last_name'],
		"first_name" => $arr['first_name'],
		"birth_date" => $arr['birth_date'],
		"gender"     => $arr['gender']
		);
		$this->insert($data);
	}

	public function getEmploye($emp_no)
	{
		$data = $this->select()->where("emp_no = ?",$emp_no); 
		return $this->fetchRow($data);
	}

	public function updateEmploye($arr)
	{
		$data = array(
			"last_name"  => $arr['last_name'],
			"first_name" => $arr['first_name'],
			"birth_date" => $arr['birth_date'],
			"gender"     => $arr['gender']
		);
		$where="emp_no ='".$arr["emp_no"]."'";
		$this->update($data,$where);
	}
	function deleteEmploye($emp_no)
	{
		$where="emp_no ='".$emp_no."'";
		$this->delete($where);
	}


}

