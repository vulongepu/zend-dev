<?php

class Application_Form_CreateEmploye extends Zend_Form
{

    public function init()
    {
        parent::init();
        $this->setAction('')
        ->setMethod('post')
        ->setAttrib('id', 'basic-form')
        ->setAttrib('enctype','multipart/form-data');

        $emp_no = $this->createElement('hidden','emp_no');
        $last_name = $this->createElement('text','last_name',array( 'label'=>"Họ",
                                          'required'=>"true", 
                                          'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 16))),
                                    ));
        $first_name = $this->createElement('text','first_name',array( 'label'=>"Tên",
                                           'required'=>"true",
                                           'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 14)))
                                    ));
        $birth_date = $this->createElement('text','birth_date',array( 'label'=>"Ngày sinh",
                                          'required'=>"true",
                                           'validators' => array(
                                              array(
                                                  'name' => 'date',
                                                   'options' => array(
                                                         'format' => 'd/m/Y',
                                                    ),
                                                ),
                                          ),
                                    ));
        $gender=$this->createElement('select','gender',array( 'label'=>"Giới tính", 'multioptions'=>array("M"=>"Nam",
                                            "F"=>"Nữ"),'value'=>'M'
                                         ));

        $gender->setValue(array('M', 'F'));
        $submit = $this->createElement('submit',"Add", array('label' => 'Submit'));
        
        $this->addElement($emp_no)
            ->addElement($last_name)
            ->addElement($first_name)
            ->addElement($birth_date)
            ->addElement($gender)
            ->addElement($submit);

    }

}

