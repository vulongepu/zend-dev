<?php

class IndexController extends Labo_Controller_Action
{

    public function indexAction()
    {
		$employees = new Application_Model_Employe();
		$employees = $employees->getList()->toArray();
		$this->view->employees =$employees;
    }

    public function getForm()
    {
        $form = new Zend_Form();
		$form->setAction('/index/store')
             ->setMethod('post');
             
		$last_name  = $form->createElement('text', 'last_name',['label' => 'Họ']);
		$first_name = $form->createElement('text', 'first_name',['label' => 'Tên']);
		$birth_date = $form->createElement('text', 'birth_date',['label' => 'Ngày sinh']);
		$gender     = $form->createElement('select','gender',
            ['label'=>"Giới tính",'multioptions'=>["M"=>"Nam","F"=>"Nữ"],'value'=>'1']);
            
		$form->addElement($last_name)
		     ->addElement($first_name)
		     ->addElement($birth_date)
		     ->addElement($gender)
             ->addElement('submit', 'login', array('label' => 'Thêm'));
             
        return $form;
    }

    public function addAction()
    {

	    $request = $this->getRequest();
        $form    = new Application_Form_CreateEmploye();

        if ($this->getRequest()->isPost ()) {
            if ($form->isValid($request->getPost())) {
                $data = $this->_request->getParams();
                $employe = new Application_Model_Employe();
				$employe->createEmploye($data);
                return $this->_helper->redirector('index');
            }
        }
 
        $this->view->form = $form;

    }

    public function editAction()
    {
        $emp_no = $this->_request->getParam('emp_no'); 
        $form = new Application_Form_CreateEmploye();
        $employe = new Application_Model_Employe(); 
        if(!$this->_request->ispost()){
        $data = $employe->getEmploye($emp_no)->toArray(); 
        $form->populate($data);
        }else{
            $data = $this->_request->getParams();
            $employe->updateEmploye($data);
            $this->_redirect("index"); 
        }
        $this->view->form = $form;
    }
    
    public function delAction()
    {
        $emp_no = $this->_request->getParam('emp_no');  
        $employe = new Application_Model_Employe; 
        $employe->deleteEmploye($emp_no); 
        $this->_redirect("index"); 
        $this->_helper->viewRenderer->setNoRender(true);
    }


}

